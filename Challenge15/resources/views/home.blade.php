@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <strong> Name: </strong> {{ Auth::user()->name }} <br>
                    <strong> LastName: </strong> {{ Auth::user()->lastname }} <br>
                    <strong> Username: </strong> {{ Auth::user()->username }} <br>
                    <strong> Email: </strong> {{ Auth::user()->email }} <br>
                    <strong> Phone Number: </strong> {{ Auth::user()->phoneNumber }}
                    <br>
                    <hr>
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
