<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;

class CategoryController extends Controller
{
    public function categories()
    {
        $categories = Category::all();
        return view('posts.create', compact('categories'));
    }

    public function categoriesEdit()
    {
        $categories = Category::all();
        return view('posts.edit', compact('categories'));
    }
}
