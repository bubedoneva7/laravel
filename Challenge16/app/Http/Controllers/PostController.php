<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show', 'display']);
    }

    public function index()
    {
        return view('posts.show');
    }

    public function show()
    {
        $posts = Post::latest()->get();
        return view('posts.show', compact('posts'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {
        Post::create([
            'title' => request('title'),
            'image' => request('image'),
            'subtitle' => request('subtitle'),
            'body' => request('body'),
            'category_id' => request('category'),
            'user_id' => auth()->id()
        ]);

    return redirect('posts/show');
    }

    public function display(Post $post)
    {
        return view('posts.display', compact('post'));
    }

    //------------------------------------------ EDIT----------------------------------------------------------------

    public function edit(Request $request, $id)
    {
        $post = Post::where('id', $request->id)->first();
        return view('posts.edit', compact('post'));
    }

    public function update(Request $request, $id)
    {
        // $post = Post::where('id', $id)->update([
        //     'title' => request('title'),
        //     'image' => request('image'),
        //     'subtitle' => request('subtitle'),
        //     'body' => request('body'),
        //     'category_id' => request('category')
        // ]);

        $post = Post::find($id);

        $post->category_id = $request->category;
        $post->title = $request->title;
        $post->image = $request->image;
        $post->body = $request->body;
        $post->save();

        return redirect()->back();
    }

    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();

        return redirect()->back();
    }

}
