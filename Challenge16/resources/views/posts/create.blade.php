@extends('layouts.app')

@section('content')

<form action="/posts" method="POST">
    {{ csrf_field() }}

    <div class="col-md-4 form-group mx-auto">
        <label for="category">Choose Category: (general/entertainment/sports/movies/politics/cars)</label>
        {{-- <input type="category" class="form-control" name="category">
         --}}

         <select name="category" id="category">

             @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name}}</option>
             @endforeach

         </select>
    </div>

    <div class="col-md-4 form-group mx-auto">
        <label for="title">Insert Title: </label>
        <input type="text" class="form-control" name="title">
    </div>

    <div class="col-md-4 form-group mx-auto">
        <label for="image">Insert Image: </label>
        <input type="url" class="form-control" name="image">
    </div>

    <div class="col-md-4 form-group mx-auto">
        <label for="body">Insert Body: </label>
        <textarea name="body" id="body" class="form-control" name="body"></textarea>
    </div>

    <div class="col-md-4 form-group mx-auto">
        <input type="submit" value="Submit" class="btn btn-block btn-primary">
    </div>


</form>

@endsection
