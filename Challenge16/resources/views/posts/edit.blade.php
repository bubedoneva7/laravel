@extends('layouts.app')

@section('content')


<form action="/posts/{{ route('edit', ['id' => $post->id])}}" method="POST">
    {{ csrf_field() }}

    <div class="col-md-4 form-group mx-auto">
        <label for="category">Choose Category: (general/entertainment/sports/movies/politics/cars)</label>

         <select name="category" id="category">

             @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name}}</option>
             @endforeach

         </select>
    </div>

    <div class="col-md-4 form-group mx-auto">
        <label for="title">Edit Title: </label>
    <input type="text" class="form-control" name="title">
    </div>

    <div class="col-md-4 form-group mx-auto">
        <label for="image">Edit Image: </label>
        <input type="url" class="form-control" name="image">
    </div>

    <div class="col-md-4 form-group mx-auto">
        <label for="body">Edit Body: </label>
        <textarea name="body" id="body" class="form-control" name="body"></textarea>
    </div>

    <div class="col-md-4 form-group mx-auto">
        <input type="submit" value="Update" class="btn btn-block btn-primary">
    </div>


</form>

@endsection
