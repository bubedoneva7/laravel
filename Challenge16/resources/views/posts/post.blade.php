 <div class="col-md-4" style="width: 18rem;">

            <img class="card-img-top" src="{{ $post->image }}" alt="Card image cap">

    <div class="card-body">

        <h5 class="card-title">{{ $post->title }}</h5>
        <p class="text-justify"><small class="text-muted">{{ $post->user->username}} on {{ $post->created_at->toFormattedDateString() }}</small></p>
        <p>{{ $post->category->name }}</p>
        <p class="card-text">{{ $post->body }}</p>

        <a href="/posts/{{ $post->id }}" class="btn btn-primary">Read More</a>

        @if (Auth::check())

            @if (Auth::user()->id == $post->user->id || Auth::user()->is_admin == 1)

                <form action="/posts" method="post">
                    {{ csrf_field() }}

                    <div class="form-group mt-2">
                        <button type="submit" class="btn btn-danger ml-auto"><a href="{{ route('posts.delete', $post->id) }}">Delete</a></button>
                    <button type="submit" class="btn btn-success"><a href="{{ route('posts.edit', $post->id) }}">Edit</a></button>
                    </div>

                </form>

            @endif

        @endif

    </div>
</div>
