@extends('layouts.app')

    @section('content')

        <div class="container">
            <div class=" card-deck">

                @foreach ($posts as $post)

                    @include('posts.post')

                @endforeach

            </div>
        </div>

        @if (Auth::check())

            <div style="position: relative;">
                <button type="submit" class="btn btn-success btn-lg ml-auto" style="position: fixed; top: 10%; left: 5%;"><a href="create">add</a></button>
            </div>

        @endif

    @endsection
