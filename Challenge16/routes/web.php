<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('posts/show');
});

Route::get('/posts/show', 'PostController@show')->name('posts.show');

Route::get('/posts/create', 'PostController@create');

Route::get('/posts/create', 'CategoryController@categories');

Route::post('/posts', 'PostController@store');

Route::get('/posts/{post}', 'PostController@display');



Route::get('/posts/edit/{id}', 'PostController@edit')->name('posts.edit');

Route::post('/posts/edit/{id}', 'PostController@update')->name('posts.update');

Route::get('/posts/delete/{id}', 'PostController@delete')->name('posts.delete');

Route::get('/posts/edit/{id}', 'CategoryController@categoriesEdit')->name('posts.edit');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
