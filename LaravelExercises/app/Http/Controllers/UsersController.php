<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    public function show () {

        $posts = Post::simplePaginate(15);

        return view('users', compact('posts'));
    }

    public function update(Request $request, $id){

        $post = Post::where('id', $request->id)->first();

        return view('update', compact('post'));
    }
     public function edit(Request $request, $id)
     {
        $post = Post::find($id);
        $post->name= $request->name;
        $post->surname = $request->surname;
        $post->email = $request->email;
        $post->started_working = $request->started_working;
        $post->phone_number = $request->phone_number;
        $post->job_position = $request->job_position;
        $post->position = $request->position;
        $post->salary = $request->salary;

        $post->save();
        return redirect()->route('users');
    }

    public function delete($id){
        $post = Post::find($id);
        $post->delete();

        return redirect()->route('users');
    }
}
