<?php

use App\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 100; $i++) {
            $post = new Post();

            $post->name = $faker->firstName;
            $post->surname = $faker->lastName;
            $post->email = $faker->email;
            $post->phone_number = $faker->phoneNumber;
            $post->started_working = $faker->date;
            $post->job_position = $faker->jobTitle;
            $post->position = rand(0, 2);
            $post->salary = rand(1000, 10000);

            $post->save();
        }
    }
}
