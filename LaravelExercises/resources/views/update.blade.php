<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<form action="{{ route('edit',['id' => $post->id])}}" method="POST">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="name" class="form-control" id="name"  value="{{$post->name }}" name="name">

            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">LastName</label>
            <input type="text" class="form-control" id="surname" value="{{ $post->surname }}" name="surname">
            </div>

            <div class="form-group">
                    <label for="exampleInputPassword1">Email</label>
            <input type="text" class="form-control" id="surname" value="{{ $post->email }}" name="email">
            </div>

            <div class="form-group">
                    <label for="exampleInputPassword1">Started working</label>
            <input type="date" class="form-control" id="surname" value="{{ $post->started_working }}" name="started_working">
            </div>

            <div class="form-group">
                    <label for="exampleInputPassword1">Phone Number</label>
                <input type=telephone class="form-control" id="surname" value="{{ $post->phone_number }}" name="phone_number">
            </div>

            <div class="form-group">
                    <label for="exampleInputPassword1">Job position</label>
                <input type="text" class="form-control" id="surname" value="{{ $post->job_position }}" name="job_position">
            </div>

            <div class="form-group">
                    <label for="exampleInputPassword1">Position</label>
                <input type="text" class="form-control" id="surname" value="{{ $post->position }}" name="position">
            </div>

            <div class="form-group">
                    <label for="exampleInputPassword1">Salary</label>
                <input type="number" class="form-control" id="surname" value="{{ $post->salary }}" name="salary">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
             {{ csrf_field() }}
        </form>
</body>
</html>
