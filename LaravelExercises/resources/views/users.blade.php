<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

    <div class="container mt-5">
        <div class="row border-bottom">
            <div class="col-sm-1">
                <p>Name</p>
            </div>

            <div class="col-sm-1">
                <p>Surname</p>
            </div>

            <div class="col-sm-3">
                <p>Email</p>
            </div>

            <div class="col-sm-2">
                <p>Start Date</p>
            </div>

            <div class="col-sm-2">
                <p>Job position</p>
            </div>

            <div class="col-sm-2">Modify</div>
        </div>
        @foreach ($posts as $post)
            <div class="row">

                <div  class="col-sm-1">
                    {{ $post->name }}
                </div>

                <div class="col-sm-1">
                    {{ $post->surname }}
                </div>

                <div class="col-sm-3">
                    {{ $post->email }}
                </div>

                <div class="col-sm-2">
                    {{ $post->started_working }}
                </div>

                <div class="col-sm-2">
                    {{ $post->job_position }}
                </div>

                <div class="col-sm-2">
                        <button type="submit"><a href="{{ route('update', ['id' => $post->id])}}">Update</a> </button>
                        <button type="submit"><a href="{{ route('delete', ['id' => $post->id])}}">Delete</a> </button>
                </div>

            </div>
        @endforeach
        <div class="col-sm-2 mt-5 d-flex justify-content-center">
            {{ $posts->links()}}
        </div>
    </div>


{{-- <table class="list-group">
    <thead  class="list-group-item">
        <th >Name </th>
        <th class="mx-3">Surname</th>
        <tbody class="list-body">
    </thead>


        @foreach ($posts as $post)
        <tr  class="list-group-item">
            <td class="">
                {{ $post->name }}
            </td>

            <td>
                {{ $post->surname }}
            </td>
        </tr>
        @endforeach

    </tbody>

</table> --}}


</body>
</html>
