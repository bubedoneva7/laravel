<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', 'UsersController@show')->name('users');

Route::get('/update/{id}', 'UsersController@update')->name('update');
Route::get('/delete/{id}', 'UsersController@delete')->name('delete');


Route::post('/update/{id}','UsersController@edit')->name('edit');
