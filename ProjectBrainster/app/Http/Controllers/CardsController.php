<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Http\Request;

class CardsController extends Controller
{
    public function create()
    {
        $cards = Card::latest()->get();
        return view('create', compact('cards'));
    }

    public function show(Card $card)
    {

        return view('show', compact('card'));
    }

    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required|max:200'
        ]);

        Card::create(request(['image', 'title', 'subtitle', 'body']));

        // $card = new Card;

        // $card->image = request('image');
        // $card->title = request('title');
        // $card->subtitle = request('subtitle');
        // $card->body = request('body');

        // $card->save();

        return redirect()->route('create');
    }

    public function edit(Request $request, $id)
    {
        // validation
        $card = Card::where('id', $request->id)->first();

        return view('edit', compact('card'));
    }

    public function editCard(Request $request, $id)
    {
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required|max:200'
        ]);
            // validation
        $card = Card::find($id);
        $card->image = $request->image;
        $card->title = $request->title;
        $card->subtitle = $request->subtitle;
        $card->body = $request->body;

        $card->save();
        return redirect()->route('create');

    }

    public function add()
    {
        return view('add');
    }

    public function delete($id){
        // confirmation js
        $card = Card::find($id);
        $card->delete();

        return redirect()->route('create');
    }
}
