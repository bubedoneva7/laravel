<?php

use App\Admin;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $admin = new Admin();

        $admin->email = 'admin@example.com';
        $admin->password = bcrypt('qwerty');

        $admin->save();
    }
}

