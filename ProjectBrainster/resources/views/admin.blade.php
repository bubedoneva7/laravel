@extends('layouts.master')

@section('content')

<div class="container pt-5">
    <div class="d-flex row justify-content-center mt-5">
        <div class="col-sm-6 border rounded-0 mt-5 pt-5">
            <form method="POST" action="/admin">
                {{ csrf_field() }}

            <div class="form-group">
                    <label for="email">Enter Email:</label>
                    <input type="email" class="form-control" id="email" name="email">
            </div>

            <div class="form-group">
                    <label for="password">Enter Password</label>
                    <input type="password" id="password" class="form-control" name="password" ></input>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-warning btn-lg btn-block">Sign In</button>
            </div>

            <p>// Authentication is not done, please use /create</p>
        </form>
        </div>
    </div>
</div>
@endsection

