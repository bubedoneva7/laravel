
           {{-- Animations --}}
           {{-- data-aos="fade-up" data-aos-anchor-placement="center-center" data-aos-duration="500" --}}

           {{-- fix the cards to appear nexto to each other
            -fix validation when there is no card to appear error
           - add the cards to the front page --}}

        <div class="col-md-4 col-sm-12">
            <div class="card">
                <img class="card-img-top" src="{{ $card->image }}">

                <div class="card-body">
                    <h4 class="card-title text-center px-3">
                        <a href="{{ $card->id }}">
                            {{ $card->title }}</a>
                    </h4>
                    <h5 class="card-text">{{ $card->subtitle }}</h5>
                    <p class="card-text">
                            {{ $card->created_at->toFormattedDateString() }}
                        </p>
                    <p class="card-text">{{ $card->body }}</p>

                    <a href="{{ route('edit', ['id' => $card->id])}}" class="customFontEdit" ><i class="fas fa-pencil-alt"></i></a>

                    <a href="{{ route('delete', ['id' => $card->id])}}" class="customFontDelete px-2"><i class="fas fa-times "></i></a>
                </div>
            </div>
        </div>

