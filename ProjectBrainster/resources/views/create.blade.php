@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="row mt-5"></div>

            @foreach ($cards as $card)

            @include('cards')
            @endforeach
        </div>
    </div>

    <div class="col d-flex justify-content-end">
        <a class="customFontAwesome px-4 py-3" href="{{ route('add', ['id' => $card->id])}}"> <i class="fas fa-plus"></i></a>
    </div>


@endsection
