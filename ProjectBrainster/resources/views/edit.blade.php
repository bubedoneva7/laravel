@extends('layouts.master')

@section('content')


<div class="col-md-6">
        <form action="{{ route('editCard',['id' => $card->id])}}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="url">Image</label>
                <input type="text" name="image" class="form-control" value="{{ $card->image }}" >
            </div>

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" value="{{ $card->title }}">
            </div>

            <div class="form-group">
                <label for="subtitle">Subtitle</label>
                <input type="text" name="subtitle" class="form-control" value="{{ $card->subtitle }}">
            </div>

            <div class="form-group">
                <label for="body">Body</label>
                <textarea name="body" id="body" class="form-control" value="{{ $card->body }}"></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="form-control btn btn-warning">Post</button>
            </div>

            @include('layouts.errors')
        </form>
    </div>

@endsection
