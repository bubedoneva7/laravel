<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a href="FrontPage.php" class="navbar-brand"><img width="170" src="images/logo.png" alt="brainster logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
            <div class="navbar-nav text-center">
                <a href="#" class="nav-item nav-link customFont">Академија за Програмирање</a>
                <a href="#" class="nav-item nav-link customFont">Академија за Маркетинг</a>
                <a href="#" class="nav-item nav-link customFont">Блог</a>
                <a href="EmployStudents.php" class="nav-item nav-link customFont employStudents">Вработи наши студенти</a>
            </div>
        </div>
    </div>
</nav>



