<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@show')->name('home');

Route::get('/admin', 'AdminController@show');



// Route::post('/admin', 'AdminController@store');

// Route::get('/logout', 'AdminController@destroy');


Route::get('/create', 'CardsController@create')->name('create');

Route::post('/create', 'CardsController@store');

Route::get('/{card}', 'CardsController@show');

Route::get('/add/{id}', 'CardsController@add')->name('add');

Route::get('/edit/{id}', 'CardsController@edit')->name('edit');

Route::post('/edit/{id}','CardsController@editCard')->name('editCard');

Route::get('/delete/{id}', 'CardsController@delete')->name('delete');

