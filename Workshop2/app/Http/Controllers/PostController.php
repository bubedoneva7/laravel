<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show', 'display']);
        // $this->middleware('editor');
    }

    public function show()
    {
        $posts = Posts::latest()->get();
        return view('posts.show', compact('posts'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {

        Posts::create([
            'title' => request('title'),
            'subtitle' => request('subtitle'),
            'body' => request('body'),
            'user_id' => auth()->id()
            ]);

        return redirect('posts/show');
    }

    public function display(Posts $post)
    {

        return view('posts.display', compact('post'));
    }
}
