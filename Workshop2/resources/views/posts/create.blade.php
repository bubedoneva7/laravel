@extends('layouts.app')

    @section('content')

    <div class="col-sm-8">

        <h1>Publish a post</h1>

        <form method="POST" action="/posts">
             {{ csrf_field() }}

            <div class="form-group">
                  <label for="title">Title:</label>
                  <input type="title" class="form-control" id="title" name="title">
            </div>

            <div class="form-group">
                <label for="subtitle">Title:</label>
                <input type="subtitle" class="form-control" id="subtitle" name="subtitle">
          </div>

            <div class="form-group">
                  <label for="body">Body</label>
                  <textarea name="body" id="body" class="form-control" ></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Publish</button>
            </div>


        </form>

    </div>

    @endsection
