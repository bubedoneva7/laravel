<div class="blog-post">

    <h2 class="blog-post-title">
        <a href="/posts/{{ $post->id }}">
            {{ $post->title }}
        </a></h2>

        <p class="blog-post-meta">
            {{ $post->user->name }} on
            {{ $post->created_at->toFormattedDateString() }}
        </p>

        {{ $post->body }}


    @if (Auth::check() )

        @if (Auth::user()->id == $post->user->id || Auth::user()->role == 'admin')

            <form action="/posts" method="post">

                <div class="form-group">
                    <button type="submit" class="btn btn-danger">Delete</button>
                    <button type="submit" class="btn btn-success">Edit</button>
                </div>

            </form>



        @endif

    @endif

</div>

